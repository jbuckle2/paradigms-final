# Paradigms Final Project (jbuckle2 and rpairitz)

Songs File Description
=============================
The data in this project is in the form of a JSON file created by David Pots, musician and web developer.

The data for each song is formatted like this example (pulled from the actual JSON file):
    {"title":"1904","artist":"The Tallest Man on Earth","year":"2012","web_url":"http://www.songnotes.cc/songs/78-the-tallest-man-on-earth-1904",       
    "img_url":"http://fireflygrove.com/songnotes/images/artists/TheTallestManOnEarth.jpg"}

We will be mostly using the title, artist, and release year information. The web_url links to David Pots' webpage for the song, which
includes the song's guitar chords, its lyrics, and some extra information about the song. 

The link to this JSON file is: http://davidpots.com/jakeworry/017%20JSON%20Grouping,%20part%203/data.json

OO API 
=============================
 Our OO Library API provides a number of methods that help the user manipulate the data found in the songs.json file. The methods included in the API are meant to be utilized in the songsController and resetController files. Features in this API include load_songs, get_songs, get_artists, get_years, get_song, set_song, and delete_song. 

 To run the test, enter 'python3 ooapi/test_api.py' in your terminal. 
 
RESTful JSON Specification/server
============================= 

Port number: 51045

Student machine used: student04.cse.nd.edu

To use:
1. On student machine 04, run 'python3 server/server.py'
2. On another student machine, run the tests:
    'python3 server/test_songs_index.py'
    'python3 server/test_songs_keys.py'
    'python3 server/test_reset_endpoint.py'

Note: the port number and server student machine are already specified in the server and test script files.

[Rest Specification Table Link](https://drive.google.com/file/d/1F3SOGdaTzjiBIrMtVZJmfUiBgqgKbUtC/view?usp=sharing)

Running the Server/Page
=============================
To run the server, go to the server/ directory and run the following command on student machine 04:     'python3 server.py'

If 'python3' command is not recognized, run the following command:
alias python3='/escnfs/home/csesoft/2017-fall/anaconda3/bin/python3')

Then, from the project page in GitLab (https://gitlab.com/jbuckle2/paradigms-final), go to Settings > Pages and click on the link under "Access pages." Make sure this takes you to http://jbuckle2.gitlab.io/paradigms-final. This is the client, from which the user makes requests to the server at student04 at the port number specified in the previous section.

User Interaction
=============================
When first opening the site, the user is presented with four possible pages to interact with or view. First, the homepage offers
an interactive bootstrap carousel and a link to our Search function. The nav bar also has a link to the Search page, as well as 
links to the Favorites and Our Library Page. The Home button will bring the user back to the opening page. 

To search for songs, you specify the type of filter (title, artist, year) and write out the filter itself to get a list of songs
that match. To view the full library in alphabetical order, you can navigate to the page and press the button to "display library". 
Finally, to view the favorites of the two site builders (Ryan and Julia) you can click the "Our Favorites" tab. 

Complexity
=============================
The songs database we used includes hundreds of songs, and from each we collected three attributes. The database could also easily be adapted to include far more songs.

Our OO API includes 7 functions, and 'ooapi/test_api.py' tests the get, set, and delete functions. The server also has two controllers (resetController and songController) which support 8 combined requests.

The front end involves interaction between four pages, each with respective HTML and shared JavaScript files. The "Our Library" and "Search" pages each make calls to the server, and the JavaScript enables the dynamic updating of pages given responses from the server.

One of the key components of the project which increased the complexity was simply the aesthetic and making sure everything from the images and buttons to the main carousel on the home page especially were dynamically sized and formatted in a visually-pleasing manner. Another challenge we faced was issuing the GET request from the "Our Library" page. Similarly, we had to pay extra care on the "Search" page to allow the user to select the song field (title, artist, or release year) by which they wanted to filter results using the inputted search term.

Presentation and Image links
=============================
Demo:           https://youtu.be/izJEC__8rk8

Walkthrough:    https://youtu.be/jNmUqW8MPSA

Slides:         https://docs.google.com/presentation/d/1SBbPFM-tzQlvYHAbv6j1gTBVTsAYOq2eC9SJnYbn2R4/edit?usp=sharing

