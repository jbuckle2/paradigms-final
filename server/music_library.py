import json
import requests

class _song_database:

    def __init__(self):
        self.song_titles = dict()
        self.song_artists = dict()
        self.song_years = dict()
        #self.song_url = dict()

    def load_songs(self, songs_file):
        f = open(songs_file)
        song_data = json.load(f)
        sid = 0
        for s in song_data['songs']:
            self.song_titles[sid] = s['title']
            self.song_artists[sid] = s['artist']
            self.song_years[sid] = s['year']
            #self.song_url[sid] = s['web_url']
            sid += 1
        f.close()

    def get_songs(self):
        return self.song_titles.keys()

    def get_artists(self):
        return self.song_artists.keys()

    def get_years(self):
        return self.song_years.keys()

    def get_song(self, sid):
        try:
            stitle = self.song_titles[sid]
            sartist = self.song_artists[sid]
            syear = self.song_years[sid]
            # maybe add url
            song = list((stitle, sartist, syear))
        except Exception as ex:
            song = []
        return song

    '''
    def get_song(self, stitle):
        try:
            # maybe add url
            for s in self.song_titles:
                if stitle == self.song_titles[s]:
                    sartist = self.song_artists[sid]
                    syear = self.song_years[sid]
            song = list((stitle, sartist, syear))
        except Exception as ex:
            song = []
        return song
    '''

    def set_song(self, sid, song):
        self.song_titles[sid] = song[0]
        self.song_artists[sid] = song[1]
        self.song_years[sid] = song[2]

    def delete_song(self, sid):
        del(self.song_titles[sid])
        del(self.song_artists[sid])
        del(self.song_years[sid])


if __name__ == "__main__":
    sdb = _song_database()

    # load songs works
    sdb.load_songs('songs.json')

    # get song works
    song = sdb.get_song(2)
    print(song[0])

    song[0] = 'ABC'
    sdb.set_song(2, song)

    song = sdb.get_song(2)
    print(song[0])
