import cherrypy
import re, json
from music_library import _song_database

class ResetController(object):

    def __init__(self, sdb=None):
        if sdb is None:
            self.sdb = _song_database()
        else:
            self.sdb = sdb


    def PUT_INDEX(self):
        '''when PUT request comes in to /reset/ endpoint, then the song database is reloaded'''
        output = {'result':'success'}

        data = json.loads(cherrypy.request.body.read().decode())

        self.sdb.__init__()
        self.sdb.load_songs('songs.json')

        return json.dumps(output)

    def PUT_KEY(self, song_id):
        '''when PUT request comes in for /reset/song_id endpoint, then that song is reloaded and updated in sdb'''
        output = {'result':'success'}
        sid = int(song_id)

        try:
            data = json.loads(cherrypy.request.body.read().decode())

            sdbtmp = _song_database()
            sdbtmp.load_songs('songs.json')

            song = sdbtmp.get_song(sid)
            
            self.sdb.set_song(sid, song)

        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)

# vim: set sts=4 sw=4 ts=8 expandtab ft=python: