import unittest
import requests
import json

class TestSongs(unittest.TestCase):

    SITE_URL = 'http://student04.cse.nd.edu:51045' # replace with your port number and 
    print("testing for server: " + SITE_URL)
    SONGS_URL = SITE_URL + '/songs/'
    RESET_URL = SITE_URL + '/reset/'

    def reset_data(self):
        s = {}
        r = requests.put(self.RESET_URL, data = json.dumps(s))


    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False

    def test_songs_get_key(self):
        self.reset_data()
        song_id = 104
        r = requests.get(self.SONGS_URL + str(song_id))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['title'], 'Tangled Up in Blue')
        self.assertEqual(resp['artist'], 'Bob Dylan')
        self.assertEqual(resp['year'], '1975')

    # changing song info of an id
    def test_songs_put_key(self):
        self.reset_data()
        song_id = 80

        r = requests.get(self.SONGS_URL + str(song_id))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['title'], 'No Surprises')
        self.assertEqual(resp['artist'], 'Radiohead')
        self.assertEqual(resp['year'], '1997')

        s = {}
        s['title'] = 'Paradigms'
        s['artist'] = 'Shreya Kumar'
        s['year'] = '2020'
        r = requests.put(self.SONGS_URL + str(song_id), data = json.dumps(s))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.SONGS_URL + str(song_id))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['title'], s['title'])
        self.assertEqual(resp['artist'], s['artist'])
        self.assertEqual(resp['year'], s['year'])

    def test_songs_delete_key(self):
        self.reset_data()
        song_id = 80

        s = {}
        r = requests.delete(self.SONGS_URL + str(song_id), data = json.dumps(s))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.SONGS_URL + str(song_id))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'error')
        #self.assertEqual(resp['message'], 'song not found')

if __name__ == "__main__":
    unittest.main()

