import unittest
import requests
import json

class TestReset(unittest.TestCase):

    SITE_URL = 'http://student04.cse.nd.edu:51045' # replace with your port id
    print("Testing for server: " + SITE_URL)
    RESET_URL = SITE_URL + '/reset/'

    def test_put_reset_index(self):
        s = {}
        
        r = requests.put(self.RESET_URL, data=str(s))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.SITE_URL + "/songs/80")
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['title'], 'No Surprises')

    def test_put_reset_key(self):
        s = {}
        
        r = requests.put(self.RESET_URL+"/80", data=str(s))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.SITE_URL + "/songs/80")
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['title'], 'No Surprises') 
	

if __name__ == "__main__":
    unittest.main()

