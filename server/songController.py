import cherrypy
import re, json
from music_library import _song_database

class SongController(object):

        def __init__(self, sdb=None):
            if sdb is None:
                self.sdb = _song_database()
            else:
                self.sdb = sdb
            
            self.sdb.load_songs('songs.json')

        def GET_KEY(self, song_id):
            '''when GET request for /songs/stitle comes in, then we respond with json string'''
            output = {'result':'success'}
            song_id = int(song_id)

            try:
                song = self.sdb.get_song(song_id)
                if song is not None:
                    output['id'] = song_id
                    output['title'] = song[0]
                    output['artist'] = song[1]
                    output['year'] = song[2]
                else:
                    output['result'] = 'error'
                    output['message'] = 'song not found'
            except Exception as ex:
                output['result'] = 'error'
                output['message'] = str(ex)

            return json.dumps(output)
        
        '''
        def GET_KEY(self, stitle):
            output = {'result':'success'}

            try:
                song = self.sdb.get_song(stitle)
                if song is not None:
                    output['title'] = song[0]
                    output['artist'] = song[1]
                    output['year'] = song[2]
                else:
                    output['result'] = 'error'
                    output['message'] = 'song not found'
            except Exception as ex:
                output['result'] = 'error'
                output['message'] = str(ex)

            return json.dumps(output)
        '''

        def PUT_KEY(self, song_id):
            '''when PUT request for /songs/song_id comes in, then we change that song in the sdb'''
            output = {'result':'success'}
            song_id = int(song_id)

            data = json.loads(cherrypy.request.body.read().decode('utf-8'))

            song = list()
            song.append(data['title'])
            song.append(data['artist'])
            song.append(data['year'])

            self.sdb.set_song(song_id, song)

            return json.dumps(output)

        def DELETE_KEY(self, song_id):
            '''when DELETE for /songs/song_id comes in, we remove just that song from sdb'''
            output = {'result':'success'}
            song_id = int(song_id)
            self.sdb.delete_song(song_id)
            
            return json.dumps(output)

        def GET_INDEX(self):
            '''when GET request for /songs/ comes in, we respond with all the song information in a json str'''
            output = {'result':'success'}
            output['songs'] = []

            try:
                for sid in self.sdb.get_songs():
                    song = self.sdb.get_song(sid)
                    curSong = {'id':sid, 'title':song[0], 'artist':song[1], 'year':song[2]}
                    output['songs'].append(curSong)
            except Exception as ex:
                    output['result'] = 'error'
                    output['message'] = str(ex)
            return json.dumps(output)

        def POST_INDEX(self):
            '''when POST for /songs/ comes in, we take title and genres from body of request, and respond with the new song_id and more'''
            output = {'result':'success'}
            output['id'] = max(self.sdb.get_songs()) + 1

            data = json.loads(cherrypy.request.body.read().decode('utf-8'))

            song = list()
            song.append(data['title'])
            song.append(data['artist'])
            song.append(data['year'])

            self.sdb.set_song(output['id'], song)
            return json.dumps(output) 


        def DELETE_INDEX(self):
            '''when DELETE for /songs/ comes in, we remove each existing song from sdb object'''
            output = {'result':'success'} 
            data = json.loads(cherrypy.request.body.fp.read().decode('utf-8')) 
            try:
                for sid in list(self.sdb.get_songs()):
                    self.sdb.delete_song(sid)
            except Exception as ex:
                    output['result'] = 'error'
                    output['message'] = str(ex)
    
            return json.dumps(output)

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
