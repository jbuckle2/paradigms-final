import cherrypy
from songController import SongController
from resetController import ResetController
from music_library import _song_database

class optionsController:
    def OPTIONS(self, *args, **kwargs):
        return ""

def CORS():
    cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
    cherrypy.response.headers["Access-Control-Allow-Methods"] = "GET, PUT, POST, DELETE, OPTIONS"
    cherrypy.response.headers["Access-Control-Allow-Credentials"] = "true"

def start_service():
    dispatcher = cherrypy.dispatch.RoutesDispatcher()

    sdb = _song_database()

    songController     = SongController(sdb=sdb) 
    resetController     = ResetController(sdb=sdb)

    dispatcher.connect('song_get', '/songs/:song_id', controller=songController, action = 'GET_KEY', conditions=dict(method=['GET']))
    dispatcher.connect('song_put', '/songs/:song_id', controller=songController, action = 'PUT_KEY', conditions=dict(method=['PUT']))
    dispatcher.connect('song_delete', '/songs/:song_id', controller=songController, action = 'DELETE_KEY', conditions=dict(method=['DELETE']))
    dispatcher.connect('song_index_get', '/songs/', controller=songController, action = 'GET_INDEX', conditions=dict(method=['GET']))
    dispatcher.connect('song_index_post', '/songs/', controller=songController, action = 'POST_INDEX', conditions=dict(method=['POST']))
    dispatcher.connect('song_index_delete', '/songs/', controller=songController, action = 'DELETE_INDEX', conditions=dict(method=['DELETE']))

    dispatcher.connect('reset_put', '/reset/:song_id', controller=resetController, action = 'PUT_KEY', conditions=dict(method=['PUT']))
    dispatcher.connect('reset_index_put', '/reset/', controller=resetController, action = 'PUT_INDEX', conditions=dict(method=['PUT']))

    # CORS
    dispatcher.connect('song_key_options', '/songs/:song_id', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('song_options', '/songs/', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('reset_key_options', '/reset/:song_id', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('reset_options', '/reset/', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))


    conf = {
	'global': {
        'server.thread_pool': 5, # optional argument
	    'server.socket_host': 'student04.cse.nd.edu', # 
	    'server.socket_port': 51045, #change port number to your assigned
	    },
	'/': {
	    'request.dispatch': dispatcher,
        'tools.CORS.on':True,
	    }
    }

    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config=conf)
    cherrypy.quickstart(app)

# end of start_service


if __name__ == '__main__':
    cherrypy.tools.CORS = cherrypy.Tool('before_finalize', CORS)
    start_service()
