import unittest
import requests
import json

class TestSongsIndex(unittest.TestCase):

    SITE_URL = 'http://student04.cse.nd.edu:51045' # replace with your assigned port id
    print("Testing for server: " + SITE_URL)
    SONGS_URL = SITE_URL + '/songs/'
    RESET_URL = SITE_URL + '/reset/'

    def reset_data(self):
        s = {}
        r = requests.put(self.RESET_URL, json.dumps(s))

    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False

    def test_songs_index_get(self):
        self.reset_data()
        r = requests.get(self.SONGS_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())

        testsong = {}
        songs = resp['songs']
        for song in songs:
            if song['id'] == 104:
                testsong = song

        self.assertEqual(testsong['title'], 'Tangled Up in Blue')
        self.assertEqual(testsong['artist'], 'Bob Dylan')
        self.assertEqual(testsong['year'], '1975')

    def test_songs_index_post(self):
        self.reset_data()

        s = {}
        s['title'] = 'Paradigms'
        s['artist'] = 'Shreya Kumar'
        s['year'] = '2020'
        r = requests.post(self.SONGS_URL, data = json.dumps(s))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')
        self.assertEqual(resp['id'], 128)  # not sure about this

        r = requests.get(self.SONGS_URL + str(resp['id']))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['title'], s['title'])
        self.assertEqual(resp['artist'], s['artist'])

    def test_songs_index_delete(self):
        self.reset_data()

        s = {}
        r = requests.delete(self.SONGS_URL, data = json.dumps(s))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.SONGS_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        songs = resp['songs']
        self.assertFalse(songs)

if __name__ == "__main__":
    unittest.main()

