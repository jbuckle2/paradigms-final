import unittest
from music_library import _song_database

class TestAPI(unittest.TestCase):
    
    sid = 80
    stitle = 'No Surprises'
    sdb = _song_database() 

    def reset_song(self):
        song = list((self.stitle, 'Radiohead', '1997'))
        self.sdb.set_song(self.sid, song)

    def test_get_song(self):
        '''test get_song method'''
        self.reset_song()
        song = self.sdb.get_song(self.sid)
        self.assertEqual(song[0], self.stitle)

    def test_set_song(self):
        self.reset_song()
        song = self.sdb.get_song(self.sid)
        song[0] = 'Paradigms'
        song[1] = 'Shreya Kumar'
        self.sdb.set_song(self.sid, song)
        song = self.sdb.get_song(self.sid)
        self.assertEqual(song[0], 'Paradigms')
        self.assertEqual(song[1], 'Shreya Kumar')
    
    def test_delete_song(self):
        self.reset_song()
        self.sdb.delete_song(self.sid)
        song = self.sdb.get_song(self.sid)
        self.assertEqual(song, [])
        #self.assertEqual(type(song), 'NoneType')


if __name__ == "__main__":
    unittest.main()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python: