console.log('page load - entered main.js');

var submitButton = document.getElementById('bsr-submit-button');
submitButton.onmouseup = getSearchResult;

function getSearchResult(){
    console.log('entered getSearchResult!');
    var selparam = document.getElementById('select-parameter').selectedIndex;
    var paramType = document.getElementById('select-parameter').options[selparam].value;
    // call displayinfo
    var param = document.getElementById("title-text").value;
    console.log('Title you entered is ' + param);
    makeNetworkCallSongList(paramType, param);
} // end of get form info

function makeNetworkCallSongList(paramType, param){
    console.log('entered make nw call' + param);
    
    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = "http://student04.cse.nd.edu:51045/songs/";
    xhr.open("GET", url, true); // 2 - associates request attributes with xhr

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log(xhr.responseText);
        // do something
        updateSearch(paramType, param, xhr.responseText);
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(null) // last step - this actually makes the request
}

function updateSearch(paramType, param, response_text){
    var response_json = JSON.parse(response_text);
    response_json = response_json['songs'];
    // update a label
    var label1 = document.getElementById("response-line1");
    var curSong;
    var songInfo;

    label1.innerHTML = '';

    console.log("param:");
    console.log(param);
    console.log("paramType:");
    console.log(paramType);

    switch(paramType){
        case 'title':
	    var songFound = 0;
            for (var i = 0; i < response_json.length; i++){
                curSong = response_json[i];
                if (!param.localeCompare(curSong['title'])){
		    songFound = 1;
		    var tag = document.createElement("p");
                    songInfo = curSong;
            	    sartist = songInfo['artist'];
           	    syear = songInfo['year'];
        	    var songData =  '"' + param + '" by ' + sartist + ' (' + syear + ')';
                    var songText = document.createTextNode(songData);
		    tag.appendChild(songText);
                    label1.appendChild(tag);
                }
            }
            
            if(songFound == 0){
                label1.innerHTML = 'Sorry, we could not find a song by that title in our library.';
            } 
            break;
        case 'artist':
            var artistFound = 0;
            for (var i = 0; i < response_json.length; i++){
                curSong = response_json[i];
                if (!param.localeCompare(curSong['artist'])){
                    artistFound = 1;
                    var tag = document.createElement("p");
                    stitle = curSong['title'];
                    syear = curSong['year'];
        	        var songData =  '"' + stitle + '" by ' + param + ' (' + syear + ')';
                    var songText = document.createTextNode(songData);
                    tag.appendChild(songText);
                    label1.appendChild(tag);
                }
            }
            if (artistFound == 0) {
                label1.innerHTML = 'Sorry, we could not find your artist in our library.';
            }
            break;
        case 'year':
            var yearFound = 0;
            for (var i = 0; i < response_json.length; i++){
                curSong = response_json[i];
                if (!param.localeCompare(curSong['year'])){
                    yearFound = 1;
                    var tag = document.createElement("p");
                    stitle = curSong['title'];
                    sartist = curSong['artist'];
        	        var songData =  '"' + stitle + '" by ' + sartist + ' (' + param + ')';
                    var songText = document.createTextNode(songData);
                    tag.appendChild(songText);
                    label1.appendChild(tag);
                }
            }
            if (yearFound == 0) {
                label1.innerHTML = 'Sorry, no songs in our library were released during that year.';
            } 
            break;
    }
    

}

var libButton = document.getElementById("bsr-library-button");
libButton.onmouseup = getLibrary;

function getLibrary(){
    console.log('entered getLibrary');
    makeNetworkCallLibrary();
}

function makeNetworkCallLibrary(){
    console.log('entered make nw call library');
    
    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = "http://student04.cse.nd.edu:51045/songs/";
    xhr.open("GET", url, true); // 2 - associates request attributes with xhr

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log(xhr.responseText);
        // do something
        updateLibrary(xhr.responseText);
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(null) // last step - this actually makes the request 
}

function updateLibrary(response_text){
    
    var response_json = JSON.parse(response_text);
    response_json = response_json['songs'];
    // update a label
    var label1 = document.getElementById("response-line1-lib");

    for (var i = 0; i < response_json.length; i++){
        var tag = document.createElement("p");
        var curSong = response_json[i];
        stitle = curSong['title'];
        sartist = curSong['artist']; 
        syear = curSong['year'];
        var songData =  '"' + stitle + '" by ' + sartist + ' (' + syear + ')';
        var songText = document.createTextNode(songData);
        tag.appendChild(songText);
        label1.appendChild(tag);
    }
}
