console.log("entered main-lib.js");

var libButton = document.getElementById("bsr-library-button-1");
//var libButton = document.getElementById("library-button");
console.log('declared buttons - line 4');
libButton.onmouseup = getLibrary;

function getLibrary(){
    console.log('entered getLibrary');
    makeNetworkCallLibrary();
}

function makeNetworkCallLibrary(){
    console.log('entered make nw call library');
    
    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = "http://student04.cse.nd.edu:51045/songs/";
    xhr.open("GET", url, true); // 2 - associates request attributes with xhr

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log(xhr.responseText);
        // do something
        updateLibrary(xhr.responseText);
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(null) // last step - this actually makes the request 
}

function updateLibrary(response_text){
    
    var response_json = JSON.parse(response_text);
    response_json = response_json['songs'];
    // update a label
    var label1 = document.getElementById("response-line1-lib");

    for (var i = 0; i < response_json.length; i++){
        var tag = document.createElement("p");
        var curSong = response_json[i];
        stitle = curSong['title'];
        sartist = curSong['artist']; 
        syear = curSong['year'];
        var songData =  '"' + stitle + '" by ' + sartist + ' (' + syear + ')';
        var songText = document.createTextNode(songData);
        tag.appendChild(songText);
        label1.appendChild(tag);
    }
}
